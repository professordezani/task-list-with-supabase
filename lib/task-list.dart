import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

// https://supabase.com/docs/guides/getting-started/quickstarts/flutter
// https://supabase.com/docs/guides/getting-started/tutorials/with-flutter
// https://supabase.com/docs/reference/dart/initializing

class TaskListPage extends StatelessWidget {
  TaskListPage({super.key});
  final supabase = Supabase.instance.client;

  @override
  Widget build(BuildContext context) {
    // https://supabase.com/docs/guides/realtime/postgres-changes#replication-setup
    final _stream = supabase
      .from('tasks')
      .stream(primaryKey: ['id'])
      .order('name', ascending: true);

    return Scaffold(
      appBar: AppBar(title: Text("Tasks")),
      body: StreamBuilder<List<Map<String, dynamic>>>(
        stream: _stream,
        builder: (context, snapshot) {
          if (!snapshot.hasData | snapshot.hasError) {
            return const Center(child: CircularProgressIndicator());
          }
          final tasks = snapshot.data!;

          return ListView(
            children: tasks
                .map(
                  (task) => Dismissible(
                    background: Container(color: Colors.red),
                    key: Key(task['id'].toString()),
                    onDismissed: (_) async {
                        await supabase
                          .from('tasks')
                          .delete()
                          .match({ 'id': task['id'] });
                      },
                    child: CheckboxListTile(
                      title: Text(task['name']),
                      value: task['finished'],
                      onChanged: (value) async {
                        await supabase
                          .from('tasks')
                          .update({ 'finished': value })
                          .match({ 'id': task['id'] });
                      },
                    ),
                  ),
                )
                .toList(),
          );
        }
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.of(context).pushNamed('/create'),
      ),
    );
  }
}

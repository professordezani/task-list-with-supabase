import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class TaskCreatePage extends StatelessWidget {
  TaskCreatePage({super.key});
  final txtNameCtrl = TextEditingController();
  final supabase = Supabase.instance.client;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New Task")
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            TextField(
              controller: txtNameCtrl,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: "Description"
              ),
            ),
            Container(
              width: double.infinity,
              child: ElevatedButton(
                child: Text("Salvar"),
                onPressed: () async {
                  await supabase
                   .from('tasks')
                   .insert(
                    {
                      'name': txtNameCtrl.text,
                      'finished': false
                    });
                  Navigator.of(context).pop();
                }
              ),
            ),
          ],
        ),
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:todo_list_with_supabase/task-create.dart';
import 'package:todo_list_with_supabase/task-list.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Supabase.initialize(
    url: 'https://rwxtokjhkcbkxaxgzedl.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJ3eHRva2poa2Nia3hheGd6ZWRsIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTgzMTc4NDksImV4cCI6MjAxMzg5Mzg0OX0.A_Ekd2H5GACb1J9f2zPxcuNz8WprS8llfrVopCeiZf8',
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        "/list":(context) => TaskListPage(),
        "/create":(context) => TaskCreatePage(),
      },
      initialRoute: '/list',
    );
  }
}

// 3M4f9K6oynwE8VuI